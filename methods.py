from loguru import logger

from helpers import Url, rem_none_value, valid_url
from config import BASE_URL


class UrlMethods:
    @staticmethod
    def visit(uid):
        url = Url.get(id=uid)
        if url:
            url.views += 1
            return url.url

        return None

    def views(self, **kwargs):
        url = Url.get(id=kwargs['id'])

        if url:
            res = dict(
                ok=True,
                res=dict(
                    id=url.id,
                    views=url.views
                )
            )

        else:
            logger.opt(lazy=True).debug(f"url id: '{kwargs.get('id')}' dose not exist")
            res = self.invalid_url(url)
        return res

    def new_url(self, url: str, user_id: str = None, short: str = None) -> dict:
        if not user_id:
            short = None

        if not valid_url(url):
            return self.invalid_url(url)

        params = dict(
            id=short,
            user_id=user_id,
            url=url
        )

        # Check if the shorten url already exist
        if user_id and short and Url.get(id=short):
            res = dict(
                ok=False,
                error=f"id: '{short}' already exist"
            )
            return res

        new = Url(**rem_none_value(params))

        res = dict(
            ok=True,
            res=dict(
                id=new.id,
                original=url,
                user_id=user_id,
                new_url=f'{BASE_URL}/{new.id}'
            )
        )
        return res

    @staticmethod
    def invalid_url(url) -> dict:
        logger.opt(lazy=True).debug(f'url: "{url}" is not valid')
        res = dict(
            ok=False,
            error='Invalid url'
        )
        return res

    def delete_item(self, **args) -> dict:
        assert args['id'], args['user_id']
        url_id, user_id = args['id'], args['user_id']
        url = Url.get(id=url_id)

        if not url:
            return self.invalid_url(url_id)  # TODO:

        elif url.user_id != user_id:
            res = dict(
                ok=False,
                error='user_id is not belong to this url'
            )

        else:
            url.delete()

            res = dict(
                ok=True,
                res=dict(
                    method='file deleted',
                    id=url_id
                )
            )

        return res
