# welcome
Hi!  
here some explanations about this api.  
you can create shorten url fast and easy..  
the default url will be a nice url like (taken from 'dogbin' repository on github ):  
`http://base_domain/gatucu` `http://base_domain/ihanus` etc..   
also you can specify a custom short url 
(you will need to add `user_id` parameter for this)  
and you can see the number of views.

## methods

#### new
Create new shorten url.
* params
   * `url` _Required_  
      the original url must starts with __http(s)://__
   * `user_id`  _Optional_   
    unique identify for user to edit, delete and customize
   * `short` _Optional_  
    custom short url. (user_id required)  
* return  
    * json object with new shorten url.

example:
```python
import requests


base_url = "http://<base_url>/api/new"

params = {
    "url": "https://www.google.com"
}

response = requests.get(base_url, params=params)

print(response.json())
```
return
```json
{
  "ok":true,
  "res":{
    "id":"owohuq",
    "new_url":"http://<base_url>/owohuq",
    "original":"https://www.google.com",
    "user_id":null
    }
}
```

#### views
Watch number of specific short url.

* params
    * `id` _Required_  
     id of shorten url.  
* return  
    * json object with numbers of views.
   
example:
```python
import requests


base_url = "http://<base_url>/api/views"

params = {
    "id": "gatucu"  # your short url id
}

response = requests.get(base_url, params=params)

print(response.json())
```

return
```json
{
  "ok":true,
  "res":{
      "id":"owohuq",
      "view":3
       }
}
```

#### delete
Delete exist shorten url

* params
    * `id` _Required_   
    id of shorten url.
    * `user_id` _Required_   
    unique identify for the shorten url owner.
* return
    * json object
 

example:

```python
import requests


base_url = "http://<base_url>/api/delete"

params = {
    "id": "gatucu",  # your short url id,
    "user_id": 1234 # your user_id
}

response = requests.get(base_url, params=params)

print(response.json())
```

return
```json
  {
    "ok":true,
    "res":{
        "id":"<id>",
        "method":"file deleted"
        }
   }
```
