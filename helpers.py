from datetime import datetime
from random import choice, randint
from urllib.parse import urlparse


from pony.orm import Database, PrimaryKey, Required, Optional

from config import SQLITE_DB_PATH, STR_LEN

db = Database()

# github.com/dogbin
_vowels = "aeiou"
_consonants = "bcdfghjklmnpqrstvwxyz"


def random_string(length: int = STR_LEN) -> str:
    start = randint(0, 1)

    return "".join(
        choice(_consonants) if i % 2 == start
        else choice(_vowels)
        for i in range(0, length)
    )


class Url(db.Entity):
    id = PrimaryKey(str, default=random_string)
    user_id = Optional(str)
    password = Optional(str)  # todo :
    url = Required(str)
    views = Required(int, default=0)
    timestamp = Required(datetime, default=datetime.now)


db.bind('sqlite', SQLITE_DB_PATH, create_db=True)
db.generate_mapping(create_tables=True)


def valid_url(url: str) -> bool:
    result = urlparse(url)  # todo
    return all([result.scheme, result.netloc])


def rem_none_value(dic: dict) -> dict:
    return dict(filter(lambda x: x[1] is not None, dic.items()))
