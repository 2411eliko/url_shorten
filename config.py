import os

SQLITE_DB_PATH = os.getenv("", "./db.sqlite")
BASE_URL = os.getenv("", "0.0.0.0:5000")
STR_LEN = os.getenv("", 6)
