import sys

from flask import Flask, redirect, jsonify, request
from loguru import logger
from pony.flask import Pony

from methods import UrlMethods
from config import BASE_URL


app = Flask(__name__)
Pony(app)

methods = UrlMethods()
logger.add(sys.stdout, level="INFO")


@app.route("/<string:u_id>")
def redirect_url(u_id):
    url = methods.visit(u_id)
    if url:
        return redirect(url)

    logger.opt(lazy=True).debug(f"url id: '{u_id}' dose not exist")
    return f"url id: '{u_id}' dose not exist"


@app.route("/api/new", methods=['get'])
def new():
    res = methods.new_url(**request.args)
    return jsonify(res)


@app.route("/api/views", methods=['get'])
def views():
    res = methods.views(**request.args)
    return jsonify(res)


@app.route("/api/delete", methods=['get'])
def delete():
    res = methods.delete_item(**request.args)
    return jsonify(res)


if __name__ == '__main__':
    app.run(host=BASE_URL)
